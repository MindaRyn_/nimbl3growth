/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import MyRecorder from './myRecorder';
import RecordedList from './recordedList';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import * as firebase from "firebase";
import indexStyles from './indexStyleSheet';

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableHighlight
} from 'react-native';

const sections = {
  record: 'record',
  list: 'list'
};

export default class Nimbl3Growth extends Component {
  constructor(props, context) {
    super(props, context);

    firebase.initializeApp({
      apiKey: "AIzaSyCJ4zEPa0H_8oKr_53HuG3Jv3iCcYVIKDM",
      authDomain: "nimbl3growth.firebaseapp.com",
      databaseURL: "https://nimbl3growth.firebaseio.com",
      storageBucket: "nimbl3growth.appspot.com",
      projectId: "nimbl3growth",
      messagingSenderId: "147115022941"
    });

    this.state = {
      currentSection: sections.record
    };
  }

  _setSection(section) {
    this.setState({currentSection: sections[section]});
  }

  _renderContent() {
    let {currentSection} = this.state;
    
    switch(currentSection) {
      case sections.record:
        return <MyRecorder firebaseStorage={firebase.storage().ref('audios')} databaseRef={firebase.database().ref()}/>;
        break;
      default:
        return <RecordedList firebase={firebase}/>;
    }
  }
  render() {
    let {currentSection} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.nav}>
          <TouchableHighlight style={currentSection === 'record' ? styles.navItemAvtived : styles.navItemAvtivedUnavtived} onPress={() => {this._setSection('record')}} >
              <Text>Record</Text>
          </TouchableHighlight>
          <TouchableHighlight style={currentSection === 'list' ? styles.navItemAvtived : styles.navItemAvtivedUnavtived} onPress={() => {this._setSection('list')}} >
              <Text>List</Text>
          </TouchableHighlight>
        </View>
        <View></View>
        {this._renderContent()}
      </View>
    );
  }
}

const styles = indexStyles;

AppRegistry.registerComponent('Nimbl3Growth', () => Nimbl3Growth);
