import {
    StyleSheet
  } from 'react-native';

export default indexStyles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#2b608a",
      width: '100%',
    },
    nav: {
      paddingTop: 50,
      justifyContent: 'center',
      flexDirection: 'row'
    },
    navItemAvtived: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      margin: 10,
      backgroundColor: "#9AC1CF",
      width: '40%',
      height: 50
    },
    navItemAvtivedUnavtived: {
      backgroundColor: "#E9E9E9",
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      margin: 10,
      width: '40%',
      height: 50
    }
  });