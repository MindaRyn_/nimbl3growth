import React, {Component} from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  ScrollView
} from 'react-native';
import {AudioUtils} from 'react-native-audio';
import Sound from 'react-native-sound';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'react-native-fetch-blob'
import recordedListStyles from './recordedListStyleSheet';

const polyfill = RNFetchBlob.polyfill

window.XMLHttpRequest = polyfill.XMLHttpRequest
window.Blob = polyfill.Blob

class RecordedList extends Component {

    constructor(props, context) {
      super(props, context);
      this.state = {
        list: []
      };

      this._fetchList = this._fetchList.bind(this);
      this._fetchMusic = this._fetchMusic.bind(this);
      this._deleteMusic = this._deleteMusic.bind(this);
    }

    componentDidMount() {
        let {firebase} = this.props;
        let _this = this;
        this._fetchList();
        firebase.database().ref().child('file_path').on('child_removed', function(childSnapshot, prevChildKey) {
            _this._fetchList();
        });
    }

    async _fetchList(){
        let {firebase} = this.props;
        var listFile = [];
        var _this = this;
        var myData = firebase.database().ref().child('file_path');
        myData.once('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.child('path').val();
                var duration = childSnapshot.child('duration').val();
                var key = childSnapshot.key;
                listFile.push({name: childData, key: key, duration: duration});
            });
            _this.setState({list: listFile})
        });
    }

    async _fetchMusic(record){
        let {firebase} = this.props;

        firebase.storage().ref('audios/'+record).getDownloadURL().then(function(url) {     
            console.log("path : "+AudioUtils.DocumentDirectoryPath)       
            RNFS.downloadFile({
                fromUrl: url,
                toFile: `${AudioUtils.DocumentDirectoryPath}/${record}`,
              }).promise.then((r) => {
                var sound = new Sound(`${AudioUtils.DocumentDirectoryPath}/${record}`, '', (error) => {
                    if (error) {
                      console.log('failed to load the sound', error);
                    }
                });
                setTimeout(() => {
                    sound.play((success) => {
                      if (success) {
                        console.log('successfully finished playing');
                      } else {
                        console.log('playback failed due to audio decoding errors');
                      }
                    });
                  }, 100);
              });
          })
    }

    async _deleteMusic(key, name){
        let {firebase} = this.props;
        firebase.database().ref('file_path').child(key).remove()
        .then(function() {
          console.log("Path Remove succeeded.")
        })
        .catch(function(error) {
          console.log("Path Remove failed: " + error.message)
        });

        firebase.storage().ref('audios').child(name).delete().then(function() {
            console.log("File Remove succeeded.")
          }).catch(function(error) {
            console.log("File Remove failed: " + error.message)
          });
    }

    render() {
        let {list} = this.state;

      return (
        <ScrollView style={styles.container} >
            {list.map((record, index) => {
                return (
                    <View style={styles.buttonContainer} key={index}>
                        <TouchableHighlight style={styles.button} onPress={() => {this._fetchMusic(record.name)}}>
                            <Text>{record.name} [{record.duration} sec.]</Text>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.deleteButton} onPress={() => {this._deleteMusic(record.key, record.name)}}>
                            <Text>Delete</Text>
                        </TouchableHighlight>
                    </View>
                );
            })}
        </ScrollView>
      );
    }
  }

  var styles = recordedListStyles;

export default RecordedList;
