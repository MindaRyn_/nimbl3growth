import {
    StyleSheet
  } from 'react-native';

export default myRecorderStyles = StyleSheet.create({
    container: {
      flex: 1
    },
    controls: {
      marginTop: 50,
      alignItems: 'center',
      flex: 1,
    },
    progressText: {
      paddingTop: 50,
      fontSize: 50,
      color: "#fff"
    },
    button: {
      padding: 20
    },
    disabledButtonText: {
      color: '#eee'
    },
    buttonText: {
      fontSize: 20,
      color: "#fff"
    },
    activeButtonText: {
      fontSize: 20,
      color: "#B81F00"
    }
  });