import {
    StyleSheet
  } from 'react-native';

export default recordedListStyles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10
    },
    button: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#fff",
        padding: 10,
    },
    deleteButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: 'red',
        borderRadius: 4,
        marginLeft: 5
    },
    buttonContainer: {
        width: '100%',
        flexDirection: 'row',
        marginBottom: 3,
    }
  });